terraform {
  cloud {
    organization = "azureps"

    workspaces {
      name = "my-example"
    }
  }
}
